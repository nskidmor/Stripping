#!/usr/bin/env python
# =============================================================================
# $Id: StdNoPIDsDownKaons.py,v 1.4 2009-07-01 18:42:29 jonrob Exp $ 
# =============================================================================
## @file  CommonParticles/StdNoPIDsDownKaons.py
#  configuration file for 'Standard NoPIDs Downstream Kaons' 
#  @author Olli LUPTON oliver.lupton@cern.ch
#  @date 2016-11-03
# =============================================================================
"""
Configuration file for 'Standard NoPIDs Downstream Kaons'
"""
__author__  = "Olli Lupton <oliver.lupton@cern.ch>"
__version__ = "CVS tag $Name: not supported by cvs2svn $, version $Revision: 1.4 $"
# =============================================================================
__all__ = (
    'StdNoPIDsDownKaons' ,
    'locations'
    )
# =============================================================================
from Gaudi.Configuration   import *
from Configurables         import NoPIDsParticleMaker 

from CommonParticles.Utils import *

## create the algorithm 
algorithm =  NoPIDsParticleMaker ( 'StdNoPIDsDownKaons'  ,
                                DecayDescriptor = 'Kaon' ,
                                Particle = 'kaon' )

# configure the track selector
selector = trackSelector ( algorithm,
                           trackTypes = ['Downstream'],
                           cuts = { "Chi2Cut" : [0,10] } )

## configure Data-On-Demand service 
locations = updateDoD ( algorithm )

## finally: define the symbol 
StdNoPIDsDownKaons = algorithm 

## ============================================================================
if '__main__' == __name__ :

    print __doc__
    print __author__
    print __version__
    print locationsDoD ( locations ) 

# =============================================================================
# The END 
# =============================================================================

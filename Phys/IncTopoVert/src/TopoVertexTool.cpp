// Include files

// from Gaudi
#include "GaudiKernel/ToolFactory.h"
#include <utility> //@TODO/FIXME this should be included in reverse.h! -- see gaudi/Gaudi!193
#include "GaudiKernel/reverse.h"

// local
#include "TopoVertexTool.h"

namespace {

int getNbSignalTrack(const TopoVertexTool::RecVertexVector& VecVtx) {
  std::set<int> signal_track_set;
  for (const auto& vtx : VecVtx ) {
    for (const auto& tr : vtx->tracks()) {
      int mother_pid = tr->info(0,0.0);
      if ( std::abs(mother_pid) ==  15 ) {
	    signal_track_set.insert( tr->info(2,-1) );
      }
    }
  }
  return signal_track_set.size();
}


int getNbSignalTrack(const TopoVertexTool::VerticesList& VtxList) {
  std::set<int> signal_track_set;
  for (const auto& cluster : VtxList ) {
    for (const auto& vtx : *cluster ) { 
      for (const auto& tr: vtx->tracks() ) {
        int mother_pid = tr->info(0,0.0);
        if ( std::abs(mother_pid) ==  15 ) {
          signal_track_set.insert( tr->info(2,-1)  );
        }
      }
    }
  }
  return signal_track_set.size();
}
}
//-----------------------------------------------------------------------------
// Implementation file for class : TopoVertexTool
//
// 2012-09-21 : Julien Cogan and Mathieu Perrin-Terrin
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( TopoVertexTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TopoVertexTool::TopoVertexTool( const std::string& type,
                                const std::string& name,
                                const IInterface* parent )
  : base_class ( type, name , parent )
{
  m_writtenOnTES = false;

  declareInterface<ITopoVertexTool>(this);

  /*  declareProperty("VertexFunctionToolType",m_vertexFunctionToolType="VertexFunctionTool");
  declareProperty("VertexFunctionToolName",m_vertexFunctionToolName="VertexFunctionTool");

  declareProperty("DistanceCalculatorToolType",m_distanceCalculatorToolType="LoKi::DistanceCalculator");
  declareProperty("DistanceCalculatorToolName",m_distanceCalculatorToolName="DistanceCalculatorTool");

  declareProperty("TrackVertexerToolType",m_trackVertexerToolType="TrackVertexer");
  declareProperty("TrackVertexerToolName",m_trackVertexerToolName="TrackVertexerTool");

  declareProperty("TwoTracksVtxChi2Max"   ,m_twoTracksVtxChi2Max   = 20.  );
  declareProperty("TwoTracksVtxVfMin"     ,m_twoTracksVtxVfMin     =  0.1 );
  declareProperty("TwoTracksVtxVfRatioMin",m_twoTracksVtxVfRatioMin=  0.1 );
  declareProperty("TrackVtxChi2Max"       ,m_TrackVtxChi2Max       = 15.  );

  /// To be passed to the vertex function tool (temporary)
  declareProperty("MaxFinderStep",m_step=0.001);

  declareProperty("MaxFinderMinGradientMag",m_max_finder_min_gradient_mag=0.01);
  declareProperty("MaxFinderMaxIteration",m_max_finder_max_iteration=10000);
  declareProperty("MaxFinderMinStep",m_max_finder_min_step=0.000001);
  declareProperty("MaxFinderMaxjump",m_max_finder_max_jump=5);

  declareProperty("ResolverCut",m_resolver_cut=0.8);
  declareProperty("ResolverMinStep",m_resolver_min_step=0.001);
  declareProperty("ResolverMaxIteration",m_resolver_max_iteration=8);

  declareProperty("DxForGradient",m_dx_for_gradient=0.0001);
  declareProperty("MC",m_MC = true);*/

 declareProperty("VertexFunctionToolType"   ,m_vertexFunctionToolType="VertexFunctionToolRootMini");
  declareProperty("VertexFunctionToolName"    ,m_vertexFunctionToolName="VertexFunctionToolRootMini");
  declareProperty("DistanceCalculatorToolType",m_distanceCalculatorToolType="LoKi::DistanceCalculator");
  declareProperty("DistanceCalculatorToolName",m_distanceCalculatorToolName="DistanceCalculatorTool");
  declareProperty("TrackVertexerToolType"     ,m_trackVertexerToolType="TrackVertexer");
  declareProperty("TrackVertexerToolName"     ,m_trackVertexerToolName="TrackVertexerTool");
  declareProperty("TwoTracksVtxChi2Max"       ,m_twoTracksVtxChi2Max   = 10.  );
  declareProperty("TwoTracksVtxVfMin"         ,m_twoTracksVtxVfMin     =  0.1 );
  declareProperty("TwoTracksVtxVfRatioMin"    ,m_twoTracksVtxVfRatioMin=  0.8 );
  declareProperty("TrackVtxChi2Max"           ,m_TrackVtxChi2Max       = 20.  );
  /// To be passed to the vertex function tool (temporary)
  declareProperty("MaxFinderStep"          ,m_step=0.01);
  declareProperty("MaxFinderMinGradientMag",m_max_finder_min_gradient_mag=0.1);
  declareProperty("MaxFinderMaxIteration"  ,m_max_finder_max_iteration=8000);
  declareProperty("MaxFinderMinStep"       ,m_max_finder_min_step=0.01);
  declareProperty("MaxFinderMaxjump"       ,m_max_finder_max_jump=5);
  declareProperty("ResolverCut"            ,m_resolver_cut=0.35);
  declareProperty("ResolverMinStep"        ,m_resolver_min_step=0.001);
  declareProperty("ResolverMaxIteration"   ,m_resolver_max_iteration=8);
  declareProperty("DxForGradient"          ,m_dx_for_gradient=0.0001);
  declareProperty("MC",m_MC = true);




}

//=============================================================================
StatusCode TopoVertexTool::initialize()
{
  const StatusCode sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  m_vertexFunction = tool<IVertexFunctionTool>(m_vertexFunctionToolType,m_vertexFunctionToolName);
  setVertexFunctionParam();

  m_distanceCalculator = tool<IDistanceCalculator>(m_distanceCalculatorToolType,m_distanceCalculatorToolName);

  m_trackVertexer = tool<ITrackVertexer>(m_trackVertexerToolType,m_trackVertexerToolName);

  return sc;
}


StatusCode TopoVertexTool::findVertices(std::vector<const LHCb::Track*> & tracks,
                                        std::vector<const LHCb::Track*> & vTr, int* n_sig_track_step)
{
  if (n_sig_track_step){
  }

  setVertexFunctionTracks(vTr);
  return findVertices(tracks);
}
StatusCode TopoVertexTool::findVertices(std::vector<const LHCb::Particle*> & particles, int* n_sig_track_step)
{
  if (n_sig_track_step){
  }
  std::vector<const LHCb::Track*> tracks; tracks.reserve(particles.size());
  for (const auto& part : particles ) {
    const auto* proto = part->proto();
    if (!proto) continue;
    tracks.push_back(proto->track());
  }
  return findVertices(tracks);
}
StatusCode TopoVertexTool::findVertices(std::vector<const LHCb::Particle*> & particles,
                                        std::vector<const LHCb::Track*> & vTr, int* n_sig_track_step)
{
  if (n_sig_track_step){
  }
    setVertexFunctionTracks(vTr);
    return findVertices(particles);
}

StatusCode TopoVertexTool::findVertices(std::vector<const LHCb::Track*> & tracks, int* n_sig_track_step)
{
  //Check that the vertex function contains some tracks
  std::vector<const LHCb::Track*>  vf_tracks ;
  getVertexFunctionTracks(vf_tracks);
  if (vf_tracks.size()==0) {
    Warning("The Vertex Function has no tracks!",StatusCode::SUCCESS,20).ignore();
  }

  cleanUpResult();
  m_writtenOnTES = false;

  // Initialize event
  if (tracks.size()==0){
    return Warning("No input track",StatusCode::SUCCESS,20);
  }

  m_tracks = tracks;

  setVertexFunctionParam(); // Just in case some parameters where changed (?, Giampi ?)

  // Start finding algorithm, splitted in 6 steps to ease debugging and method overriding
  if (!step1().isSuccess()) {
    cleanUpFull();
    return Warning("Step1 failed",StatusCode::SUCCESS,20);
  }
  if (m_MC && n_sig_track_step )     n_sig_track_step[0] = getNbSignalTrack(m_2tracks_vertices);

  if (!step2().isSuccess()) {
    cleanUpFull();
    return Warning("Step2 failed",StatusCode::SUCCESS,20);
  }
  if (m_MC && n_sig_track_step )     n_sig_track_step[1] = getNbSignalTrack(m_2tracks_vertices);

  if (!step3().isSuccess()) {
    cleanUpFull();
    return Warning("Step3 failed",StatusCode::SUCCESS,20);
  }
  if (m_MC && n_sig_track_step )     n_sig_track_step[2] = getNbSignalTrack(m_2tracks_vertices);

  if (!step4().isSuccess()) {
    cleanUpFull();
    return Warning("Step4 failed",StatusCode::SUCCESS,20);
  }
  if (m_MC && n_sig_track_step )     n_sig_track_step[3] = getNbSignalTrack(m_list_of_clusters);

  if (!step5().isSuccess()) {
    cleanUpFull();
    return Warning("Step5 failed",StatusCode::SUCCESS,20);
  }
  if (m_MC && n_sig_track_step )     n_sig_track_step[4] = getNbSignalTrack(m_vertices);

  if (!step6().isSuccess()) {
    cleanUpFull();
    return Warning("Step6 failed",StatusCode::SUCCESS,20);
  }
  if (m_MC && n_sig_track_step )     n_sig_track_step[5] = getNbSignalTrack(m_vertices);

  cleanUpTemporary();
  return StatusCode::SUCCESS;
}


StatusCode TopoVertexTool::step1()
{
  /*
    Make 2 tracks vertices from all tracks combinations.
    Keep only the ones surviving the chi2 and Vmin cuts.
  */

  for (const auto& ptrk : m_tracks ) m_2tracks_vertices_per_track.emplace( ptrk, RecVertexVector{} );

  auto end0 = std::prev(m_tracks.end());
  for (auto it_ptrk0=m_tracks.begin(); it_ptrk0!=end0 ; ++it_ptrk0){
    std::vector<const LHCb::Track *> two_tracks_container;
    two_tracks_container.push_back(*it_ptrk0);
    for (auto it_ptrk1=std::next(it_ptrk0); it_ptrk1!=m_tracks.end(); ++it_ptrk1){
      two_tracks_container.push_back(*it_ptrk1);
      auto vertex = m_trackVertexer->fit(two_tracks_container);
      if (vertex) {
        two_tracks_container.pop_back();
        if (vertex->chi2() > (2*m_twoTracksVtxChi2Max)) continue;
        if (m_vertexFunction->valueAt(*vertex) < m_twoTracksVtxVfMin) continue;
      } else {
        Warning("fit didn't converge 1",StatusCode::SUCCESS,0).ignore();
      }

      m_2tracks_vertices.push_back(vertex.release());
      m_2tracks_vertices_per_track[*it_ptrk0].push_back(m_2tracks_vertices.back());
      m_2tracks_vertices_per_track[*it_ptrk1].push_back(m_2tracks_vertices.back());
    }
  }
  return StatusCode::SUCCESS;
}
StatusCode TopoVertexTool::step2()
{
  /*
    Clean up track list for every vertex, scanning the tracks and the vertices they belong to.
  */

  // Loop over tracks (using the vertices by track map)
  for (auto& pair : m_2tracks_vertices_per_track ) {

    const LHCb::Track * track  = pair.first;
    RecVertexVector&  vertices = pair.second;

    if (vertices.size()<2) continue ;

    //FIXME: does not require a fully sorted list. The max value, combined with partition
    //       suffices...
    // Get highest vertex function of all vertices containing the current track
    m_vertexFunction->sortByValueAt(vertices);
    double highestVf = m_vertexFunction->valueAt(*(vertices.back()));

    // Remove this track from all vertices with vertex function lower than a fraction of the highest
    auto pivot = std::partition_point( vertices.begin(), vertices.end(),
                                       [&](LHCb::RecVertex* vtx) {
                 return m_vertexFunction->valueAt(*vtx) < (highestVf*m_twoTracksVtxVfRatioMin) ; 
    });
    std::for_each(vertices.begin(), pivot, [&](LHCb::RecVertex* vtx) { vtx->removeFromTracks(track); } );

    // Remove from track list the vertices the track have been removed from above
    vertices.erase(vertices.begin(),pivot);

    // Remove track from vertices which are unresolved from better vertices
    RecVertexVector check_list;
    for (auto& ivtx0 : reverse( vertices ) ) {
      auto ivtx1 = std::find_if(check_list.rbegin(), check_list.rend(),
                                [&](typename RecVertexVector::const_reference vtx1) 
                                { return ! m_vertexFunction->areResolved( *ivtx0, *vtx1); } );
      if (ivtx1 != check_list.rend()) {
          ivtx0->removeFromTracks(track);
      } else {
          check_list.push_back(ivtx0);
      }
    }
  }
  return StatusCode::SUCCESS;
}
StatusCode TopoVertexTool::step3()
{
  /*
    Remove vertex with no remaining track.
    Sort list of vertices by maximum vertex function value.
  */
  auto pivot = std::partition( m_2tracks_vertices.begin(), m_2tracks_vertices.end(),
                               [](const LHCb::RecVertex* vtx) { return !vtx->tracks().empty(); } );
  std::for_each( pivot, m_2tracks_vertices.end(), std::default_delete<LHCb::RecVertex>{} );
  m_2tracks_vertices.erase(pivot,m_2tracks_vertices.end());

  m_vertexFunction->sortByValueMax(m_2tracks_vertices);
  std::reverse(m_2tracks_vertices.begin(),m_2tracks_vertices.end());
  return StatusCode::SUCCESS;
}
StatusCode TopoVertexTool::step4()
{
  /*
    Merge vertices into clusters.
  */
  RecVertexVector remaining_vertices{ m_2tracks_vertices.begin(),m_2tracks_vertices.end() };
  while (!remaining_vertices.empty()){
    auto next_remaining_vertex = remaining_vertices.begin();
    RecVertexVector * pcluster = new RecVertexVector();
    pcluster->push_back(*next_remaining_vertex);
    m_list_of_clusters.push_back(pcluster);
    remaining_vertices.erase(next_remaining_vertex);
    addUnResolvedToCluster(*pcluster,remaining_vertices);
  }
  return StatusCode::SUCCESS;
}
StatusCode TopoVertexTool::step5()
{
  m_vertices.clear();
  /*
    Fit clusters
  */
  for (auto& cluster: m_list_of_clusters ) {
    TrackVector all_tracks;
    for (auto& vertex : *cluster ) {
      const auto& vertex_tracks = vertex->tracks();
      all_tracks.insert(all_tracks.end(),vertex_tracks.begin(),vertex_tracks.end());
    }
    std::stable_sort(all_tracks.begin(),all_tracks.end());
    auto tracks_end = std::unique(all_tracks.begin(),all_tracks.end());
    auto vertex = m_trackVertexer->fit( TrackVector{ all_tracks.begin(), tracks_end } );

    if (vertex)  m_vertices.push_back(vertex.release());
    else {
      Warning("fit didn't converge 2",StatusCode::SUCCESS,0).ignore();
    }
  }
  return StatusCode::SUCCESS;
}
StatusCode TopoVertexTool::step6()
{
  /*
    Final computation and clean up.
  */

  RecVertexVector vertices;
  // Remove tracks contributing too much to chi2, make new vertices eventually
  for (auto& vertex : m_vertices ) {
    std::unique_ptr<LHCb::RecVertex> vtx(vertex); vertex = nullptr;
    vtx = removeHighChisqTracksFromVertex( std::move(vtx) );
    if (vtx->tracks().empty()) continue;
    vertices.push_back(vtx.release());
  }

  // Clear old vertices vector (unused vertex have already been deleted in removeHighChisqTracksFromVertex)
  m_vertices.clear();

  // Sort by Max
  m_vertexFunction->sortByValueMax(vertices);

  // Remove tracks already used in better (higher vmax) vertices; Remaining vertices with at least 1 track make the final list.
  TrackVector used_tracks;
  for (const auto& vertex : reverse(vertices)) {
    TrackVector to_be_removed;
    for (const auto & track : vertex->tracks() ) {
      if (std::find(used_tracks.begin(), used_tracks.end(),track)!=used_tracks.end()){
        to_be_removed.push_back(track);
      } else {
        used_tracks.push_back(track);
      }
    }
    for (auto& track : to_be_removed) vertex->removeFromTracks(track);
    if (! vertex->tracks().empty()) m_vertices.push_back(vertex);
  }
  return StatusCode::SUCCESS;
}

void TopoVertexTool::addUnResolvedToCluster(RecVertexVector & cluster, RecVertexVector & vertices, unsigned int index)
{
  /*
    Add vertices to a cluster.
    A vertex are added to a cluster if it is unresolved with ANY vertex of the cluster.
    Unresolved vertices which are added to the cluster are removed from the input list.
    All vertices in the cluster are scanned recursivly.
  */
  RecVertexVector remaining_vertices;
  LHCb::RecVertex * vtx = cluster[index];
  for (RecVertexVector::iterator it_vertex=vertices.begin(); it_vertex<vertices.end(); ++it_vertex){
    if (! m_vertexFunction->areResolved(**it_vertex,*vtx)){
      cluster.push_back(*it_vertex);
    } else {
      remaining_vertices.push_back(*it_vertex);
    }
  }
  if ( (index+1) < cluster.size()) {
    addUnResolvedToCluster(cluster,remaining_vertices, index+1);
  }
  vertices.clear();
  vertices.insert(vertices.begin(), remaining_vertices.begin(), remaining_vertices.end());
}

std::unique_ptr<LHCb::RecVertex>  TopoVertexTool::removeHighChisqTracksFromVertex(std::unique_ptr<LHCb::RecVertex> vertex)
{
  /*
    Remove from vertex tracks with a chi2 contribution above threshold.
    The method is called recursivly to remove one track at a time.
    A new vertex is created (fitted) each time a track is removed and the previous vertex is deleted.
  */

  if ((vertex->tracks()).size()<3) return vertex ;

  TrackVector updated_tracks;

  double max_chi2=0;
  const LHCb::Track * track_to_be_removed = nullptr;
  for (const auto& track : vertex->tracks() ) {
    double chi2, impact;
    StatusCode sc = m_distanceCalculator->distance(track, vertex.get(), impact, chi2);
    if (sc.isFailure ()) {
      Error(" removeHighChisqTracksFromVertex : fail to compute distance",StatusCode::FAILURE,50).ignore();
      chi2 = max_chi2+1;
    }
    if (chi2>max_chi2) {
      if (track_to_be_removed) updated_tracks.push_back(track_to_be_removed);
      track_to_be_removed = track;
      max_chi2 = chi2;
    } else {
      updated_tracks.push_back( track );
    }
  }

  if ( ( max_chi2 > m_TrackVtxChi2Max ) && (updated_tracks.size()>1) ) {
    auto new_vertex = m_trackVertexer->fit(updated_tracks);
    if (new_vertex) {
      vertex = removeHighChisqTracksFromVertex(std::move(new_vertex));
    } else {
      Warning("fit didn't converge 3",StatusCode::SUCCESS,0).ignore();
    }
  }
  return vertex;
}


void TopoVertexTool::cleanUpTemporary()
{
  // Delete RecVertices from previous search and clear lists and maps
  std::for_each(m_2tracks_vertices.begin(), m_2tracks_vertices.end(),
                std::default_delete<LHCb::RecVertex>{});
  std::for_each( m_list_of_clusters.begin(), m_list_of_clusters.end(),
                 std::default_delete<RecVertexVector>{});
  m_2tracks_vertices.clear();
  m_2tracks_vertices_per_track.clear();
  m_list_of_clusters.clear();
}

void TopoVertexTool::cleanUpResult()
{
  if (! m_writtenOnTES) {
    std::for_each(m_vertices.begin(), m_vertices.end(), std::default_delete<LHCb::RecVertex>{} );
  }
  m_vertices.clear();
}

void TopoVertexTool::cleanUpFull()
{
  cleanUpResult();
  cleanUpTemporary();
}

void TopoVertexTool::setVertexFunctionParam()
{

  m_vertexFunction->setParam("MaxFinderStep",m_step);

  m_vertexFunction->setParam("MaxFinderMinGradientMag",m_max_finder_min_gradient_mag);
  m_vertexFunction->setParam("MaxFinderMaxIteration",m_max_finder_max_iteration);
  m_vertexFunction->setParam("MaxFinderMinStep",m_max_finder_min_step);
  m_vertexFunction->setParam("MaxFinderMaxjump",m_max_finder_max_jump);

  m_vertexFunction->setParam("ResolverCut",m_resolver_cut);
  m_vertexFunction->setParam("ResolverMinStep",m_resolver_min_step);
  m_vertexFunction->setParam("ResolverMaxIteration",m_resolver_max_iteration);

  //  m_vertexFunction->setParam("DxForGradient",m_dx_for_gradient);
}

StatusCode TopoVertexTool::writeOnTES(const std::string& location)
{
  LHCb::RecVertices * pvertices = new LHCb::RecVertices();
  put(pvertices,location);
  for (auto& vertex : m_vertices) pvertices->insert(vertex);
  m_writtenOnTES = true;

  return StatusCode::SUCCESS;
}

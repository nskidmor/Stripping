"""
Module for construction of Xb->phh Stripping Selections and StrippingLines.
Provides class Xb2phhConf, which constructs the Selections and StrippingLines
given a configuration dictionary.
Exported symbols (use python help!):
   - Xb2phhConf
"""

__author__  = ['Jan Mickelle V. Maratas', 'Stephane Monteil', 'Thomas Latham']
__date__    = '24/11/2016'
__version__ = 'v1r0'
__all__     = {'Xb2phhConf',
               'default_config'}

from Gaudi.Configuration                   import *
from PhysSelPython.Wrappers                import Selection
from StrippingConf.StrippingLine           import StrippingLine
from StrippingUtils.Utils                  import LineBuilder
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, DaVinci__N3BodyDecays

from StandardParticles                     import StdNoPIDsPions     as Pions
from StandardParticles                     import StdLooseANNProtons as Protons

default_config = {
    'NAME'        : 'Xb2phh',
    'WGs'         : ['BnoC'],
    'BUILDERTYPE' : 'Xb2phhConf',
    'CONFIG'      : {'Trk_MaxChi2Ndof'   : 3.0,
                     'Trk_MaxGhostProb'  : 0.4,
                     'Trk_MinIPChi2'     : 16.0,
                     'Trk_MinP'          : 1500.0,
                     'Trk_MinProbNNp'    : 0.05,
                     'Xb_MinSumPTppi'    : 1500.0,
                     'Xb_MinM'           : 5195.0,
                     'Xb_MaxM'           : 6405.0,
                     'Xb_MinSumPT'       : 3500.0,
                     'Xb_MinPT'          : 1500.0,
                     'Xb_MaxDOCAChi2'    : 20.0,
                     'Xb_MaxVtxChi2'     : 20.0,
                     'Xb_MinFDChi2'      : 50.0,
                     'Xb_MaxIPChi2'      : 16.0,
                     'Xb_MinDira'        : 0.9999,
                     'Prescale'          : 1.0,
                     'Postscale'         : 1.0
                     },
    'STREAMS'     : ['Bhadron']
    }

class Xb2phhConf(LineBuilder) :
    """
    Builder of Xb -> phh Stripping Selection and StrippingLine.
    Constructs Xb -> phh Selections and StrippingLines from a configuration dictionary.
    Usage:
    >>> config              = { .... }
    >>> Xb2phhConf  = Xb2phhConf('Xb2phhTest', config)
    >>> Xb2phhLines = Xb2phhConf.lines
    >>> for line in Xb2phhLines :
    >>>   print line.name(), line.outputLocation()
    The lines can be used directly to build a StrippingStream object.

    Exports as instance data members:
    selXb2phh    : Xb -> phh
    lineXb2phh   : StrippingLine made out of selXb2phh
    lines        : List of lines, [lineXb2phh]

    Exports as class data member:
    Xb2phhConf.__configuration_keys__ : List of required configuration parameters.
    """
    __configuration_keys__ = ('Trk_MaxChi2Ndof',
                              'Trk_MaxGhostProb',
                              'Trk_MinIPChi2',
                              'Trk_MinP',
                              'Trk_MinProbNNp',
                              'Xb_MinSumPTppi',
                              'Xb_MinM',
                              'Xb_MaxM',
                              'Xb_MinSumPT',
                              'Xb_MinPT',
                              'Xb_MaxDOCAChi2',
                              'Xb_MaxVtxChi2',
                              'Xb_MinFDChi2',
                              'Xb_MaxIPChi2',
                              'Xb_MinDira',
                              'Prescale',
                              'Postscale')

    def __init__(self, name, config) :

        LineBuilder.__init__(self, name, config)

        self.pion      = Pions
        self.proton    = Protons

        trkFilterP     = FilterDesktop(Code = '(MIPCHI2DV(PRIMARY) > %(Trk_MinIPChi2)s)    & \
                                               (TRGHOSTPROB        < %(Trk_MaxGhostProb)s) & \
                                               (TRCHI2DOF          < %(Trk_MaxChi2Ndof)s)  & \
                                               (P                  > %(Trk_MinP)s)         & \
                                               (PROBNNp            > %(Trk_MinProbNNp)s)' % config )

        trkFilterPi    = FilterDesktop(Code = '(MIPCHI2DV(PRIMARY) > %(Trk_MinIPChi2)s)    & \
                                               (TRGHOSTPROB        < %(Trk_MaxGhostProb)s) & \
                                               (TRCHI2DOF          < %(Trk_MaxChi2Ndof)s)  & \
                                               (P                  > %(Trk_MinP)s)' % config )

        self.myPions   = Selection( 'PionsFor'+name,
                                    Algorithm = trkFilterPi,
                                    RequiredSelections = [self.pion] )

        self.myProtons = Selection( 'ProtonsFor'+name,
                                    Algorithm = trkFilterP,
                                    RequiredSelections = [self.proton] )

        self.makeXb2phh (name, config)

        self.lineXb2phh  = StrippingLine(name+'Line',
                                         prescale         = config['Prescale'],
                                         postscale        = config['Postscale'],
                                         selection        = self.selXb2phh,
                                         RelatedInfoTools = [ { "Type"           : "RelInfoConeVariables",
                                                                "ConeAngle"      : 0.8,
                                                                "TracksLocation" : '/Event/Phys/StdNoPIDsPions',
                                                                "Variables"      : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                                                "TopSelection"   : self.selXb2phh,
                                                                "Location"       : 'P2ConeVar1' },
                                                              { "Type"           : "RelInfoConeVariables",
                                                                "ConeAngle"      : 1.0,
                                                                "TracksLocation" : '/Event/Phys/StdNoPIDsPions',
                                                                "Variables"      : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                                                "TopSelection"   : self.selXb2phh,
                                                                "Location"       : 'P2ConeVar2' },
                                                              { "Type"           : "RelInfoConeVariables",
                                                                "ConeAngle"      : 1.3,
                                                                "TracksLocation" : '/Event/Phys/StdNoPIDsPions',
                                                                "Variables"      : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                                                "TopSelection"   : self.selXb2phh,
                                                                "Location"       : 'P2ConeVar3' },
                                                              { "Type"           : "RelInfoConeVariables",
                                                                "ConeAngle"      : 1.7,
                                                                "TracksLocation" : '/Event/Phys/StdNoPIDsPions',
                                                                "Variables"      : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'],
                                                                "TopSelection"   : self.selXb2phh,
                                                                "Location"       : 'P2ConeVar4' },
                                                              { "Type"           : "RelInfoVertexIsolation",
                                                                "Location"       : "VertexIsoInfo"} ] )

        self.registerLine(self.lineXb2phh )


    def makeXb2phh (self, name, config) :
        # Define all the cuts
        _mass12CutPreVtx          = '(AM < (%s - 140)*MeV)'                                   % config['Xb_MaxM']
        _sumpt12CutPreVtx         = '( (APT1 + APT2) > %s*MeV)'                               % config['Xb_MinSumPTppi']
        _doca12chi2CutPreVtx      = '(ACHI2DOCA(1,2) < %s)'                                   % config['Xb_MaxDOCAChi2']
        _combCuts12               = _mass12CutPreVtx+' & '+_sumpt12CutPreVtx+' & '+_doca12chi2CutPreVtx

        _wmassp2KCutPreVtx        = "(AWM('p+','K-','K-') > %s*MeV)"                          % config['Xb_MinM']
        _massCutPreVtx            = '(AM             < %s*MeV)'                               % config['Xb_MaxM']
        _sumptCutPreVtx           = '( (APT1 + APT2 + APT3) > %s*MeV)'                        % config['Xb_MinSumPT']
        _ptCutPreVtx              = '(APT            > %s*MeV)'                               % config['Xb_MinPT']
        _docachi2CutPreVtx13      = '(ACHI2DOCA(1,3) < %s)'                                   % config['Xb_MaxDOCAChi2']
        _docachi2CutPreVtx23      = '(ACHI2DOCA(2,3) < %s)'                                   % config['Xb_MaxDOCAChi2']
        _massCutsPreVtx           = _massCutPreVtx+' & '+_wmassp2KCutPreVtx
        _combCuts                 = _massCutsPreVtx+' & '+_sumptCutPreVtx+' & '+_ptCutPreVtx+' & '+\
                                    _docachi2CutPreVtx13+' & '+_docachi2CutPreVtx23

        _vtxChi2CutPostVtx        = '(VFASPF(VCHI2)  < %s)'                                   % config['Xb_MaxVtxChi2']
        _fdChi2CutPostVtx         = '(BPVVDCHI2      > %s)'                                   % config['Xb_MinFDChi2']
        _diraCutPostVtx           = '(BPVDIRA        > %s)'                                   % config['Xb_MinDira']
        _ipChi2CutPostVtx         = '(BPVIPCHI2()    < %s)'                                   % config['Xb_MaxIPChi2']
        _motherCuts               = _vtxChi2CutPostVtx+' & '+_fdChi2CutPostVtx+' & '+_diraCutPostVtx+' & '+_ipChi2CutPostVtx

        _Xb                       = DaVinci__N3BodyDecays()
        _Xb.Combination12Cut      = _combCuts12
        _Xb.CombinationCut        = _combCuts
        _Xb.MotherCut             = _motherCuts
        _Xb.DecayDescriptors      = [ '[Xi_b- -> p+ pi- pi-]cc', '[Xi_b~+ -> p+ pi+ pi-]cc', '[Xi_b~+ -> p+ pi+ pi+]cc' ]
        _Xb.ReFitPVs              = True

        self.selXb2phh            = Selection(name, Algorithm = _Xb, RequiredSelections = [self.myProtons, self.myPions])


